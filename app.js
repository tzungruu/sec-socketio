// Setup basic express server
const express = require('express');
const path = require('path');
const http = require('http')
const cors = require('cors');
const bodyParser = require('body-parser');
const port = 3002; // default port: 3000

var app = express();

app.use(cors({
  origin: '*'
}));
// 創建 HTTP 伺服器並與 Socket.IO 整合
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
    transports: ["websocket", "polling"],
    credentials: true,
  },
    allowEIO3: true,
  });

// 監聽客戶端連接事件
io.on('connection', (socket) => {
  console.log('A user connected');

  // 監聽客戶端發送的訊息
  socket.on('chat message', (msg) => {
    console.log('message: ' + msg);
    // 將訊息廣播給所有客戶端
    io.emit('chat message', msg);
  });

  // 監聽客戶端斷開連接事件
  socket.on('disconnect', () => {
    console.log('User disconnected');
  });
});

app.use(bodyParser.json());

app.post('/gitlab-webhook', (req, res) => {

  const event = req.get('X-Gitlab-Event');
  const payload = req.body;

  console.log("🚀 ~ app.post ~ req:", event)

  switch (event) {
    case 'Pipeline Hook':
      console.log('Pipeline Hook:', payload);
      break;
    default:
      console.log('unknown event:', event);
  }

  // 檢查請求是否為 GitLab Webhook 請求
  // if (!payload || payload.object_kind !== 'pipeline') {
  //   return res.status(400).json({ error: 'Invalid webhook payload' });
  // }

  // // 處理 Pipeline 事件
  // const pipelineStatus = payload.object_attributes.status;
  // const pipelineId = payload.object_attributes.id;
  // const projectId = payload.project.id;

  // // 廣播 Pipeline 事件到所有已連線的 Socket.IO 客戶端
  // const pipelineEvent = `Pipeline ${pipelineId} for project ${projectId} has status: ${pipelineStatus}`;
  io.emit('pipelineEvent', payload);

  res.status(200).json({ received: true });
});

app.post('/jira-webhook', (req, res) => {

  const jiraEvent = req.get('X-Atlassian-Webhook-Identifier');
  const payload = req.body;

  console.log("🚀 ~ app.post ~ req:", jiraEvent)

  switch (jiraEvent) {
    case 'jira:issue_created':
      console.log('Jira Issue Created:', payload);
      break;
    case 'jira:issue_updated':
      console.log('Jira Issue Updated:', payload);
      break;
    case 'jira:issue_deleted':
      console.log('Jira Issue Deleted:', payload);
      break;
    default:
      console.log('unknown event:', jiraEvent);
  }

  io.emit('jiraEvent', payload);

  res.status(200).json({ received: true });
});


app.get('/events', (req, res) => {
  // 设置 Content-Type 为 text/event-stream
  res.setHeader('Content-Type', 'text/event-stream');
  res.setHeader('Cache-Control', 'no-cache');
  res.setHeader('Connection', 'keep-alive');

  // 向客户端发送数据
  const sendEvent = () => {
    const now = new Date();
    const id = now.toLocaleTimeString();

    // 格式化时间为 yyyy/MM/dd HH:MM
    const formattedTime = now
      .toLocaleString('en-GB', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false, // 24小时制
      })
      .replace(',', ''); // 去掉逗号

    // 定义并生成数据数组
    const data = [
      {
        id: 0,
        title: '人',
        subColumns: [
          { text: '刷卡(臉)就位', msg: '人工2人員未就位', status: Math.floor(Math.random() * 2) },
        ],
      },
      {
        id: 1,
        title: '機',
        subColumns: [
          { text: '治工具上線', msg: '治工未送出', status: Math.floor(Math.random() * 2) },
          { text: '設備參數配方', msg: '點膠機配方未調達', status: Math.floor(Math.random() * 2) },
          { text: '設備自檢', msg: '螺絲扭力異常', status: Math.floor(Math.random() * 2) },
        ],
      },
      {
        id: 2,
        title: '料',
        subColumns: [
          { text: '各站料況確認', msg: '螺絲缺料', status: Math.floor(Math.random() * 2) },
        ],
      },
      {
        id: 3,
        title: '法',
        subColumns: [
          { text: 'E-OI SOP', msg: 'Not Ready', status: Math.floor(Math.random() * 2) },
          {
            text: 'Mock 人工站',
            msg: 'Mock2 檢測程式為調達',
            status: Math.floor(Math.random() * 2),
          },
        ],
      },
      {
        id: 4,
        title: '測',
        subColumns: [
          {
            text: '測試設備配方',
            msg: 'FT02測試程式未調達',
            status: Math.floor(Math.random() * 2),
          },
        ],
      },
      {
        id: 5,
        title: '環',
        subColumns: [
          {
            text: '測試環境',
            msg: '測試訊息',
            status: Math.floor(Math.random() * 2),
          },
        ],
      },
    ];

    const response = {
      message: `*`,
      time: formattedTime,
      data: data,
    };

    res.write(`id: ${id}\n`);
    res.write(`data: ${JSON.stringify(response)}\n\n`);
  };

  // 每隔10秒发送一次消息
  const intervalId = setInterval(sendEvent, 60000);

  // 清理：客户端断开连接时停止发送消息
  req.on('close', () => {
    clearInterval(intervalId);
    res.end();
  });

  // 立即发送一条初始消息
  sendEvent();
});

// Routing
app.use(express.static(path.join(__dirname, 'public'))); // load static resource

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});